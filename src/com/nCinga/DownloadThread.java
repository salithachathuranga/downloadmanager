package com.nCinga;

import java.io.*;
import java.net.URL;
import java.nio.file.Path;
import java.nio.file.Paths;

public class DownloadThread implements Runnable {

    private String url;
    private String threadName;
    final static String PROJECT_LOCATION = System.getProperty("user.dir");
    final static String DOWNLOAD_LOCATION = PROJECT_LOCATION + "/src/com/nCinga/files/downloads/";

    public DownloadThread(String url, String threadName) {
        this.url = url;
        this.threadName = threadName;
    }

    public void downloadFile(String url) {
        Path path = Paths.get(url);
        Path fileName = path.getFileName();
        try (BufferedInputStream inputStream = new BufferedInputStream(new URL(url).openStream());
            FileOutputStream fileStream = new FileOutputStream(DOWNLOAD_LOCATION + fileName.toString())) {
            byte[] data = new byte[1024];
            int byteContent;
            while ((byteContent = inputStream.read(data, 0, 1024)) != -1) {
                fileStream.write(data, 0, byteContent);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public String getThreadName() {
        return threadName;
    }

    @Override
    public void run() {
        System.out.println("Started Executing: " + threadName);
        downloadFile(url);
    }

}
