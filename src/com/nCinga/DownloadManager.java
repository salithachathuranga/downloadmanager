package com.nCinga;

import java.io.*;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.Executors;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;

public class DownloadManager {

    final static String PROJECT_LOCATION = System.getProperty("user.dir");
    final static String URL_LOCATION = PROJECT_LOCATION + "/src/com/nCinga/files/urls.txt";

    static List<String> readUrls() throws IOException {
        List<String> urlList = new ArrayList<>();
        BufferedReader objReader = null;
        try {
            String strCurrentLine;
            objReader = new BufferedReader(new FileReader(URL_LOCATION));
            while ((strCurrentLine = objReader.readLine()) != null) {
                urlList.add(strCurrentLine);
            }
        } finally {
            if (objReader != null) {
                objReader.close();
            }
        }
        return urlList;
    }

    public static void main(String[] args) throws IOException, InterruptedException {
        List<String> urlList = readUrls();
        ThreadPoolExecutor executor = (ThreadPoolExecutor) Executors.newFixedThreadPool(10);
        System.out.println("Started Downloading Process...");
        for (int i = 0; i < urlList.size(); i++) {
            DownloadThread thread = new DownloadThread(urlList.get(i), ("Thread " + (i + 1)));
            System.out.println("Created: " + thread.getThreadName());
            executor.execute(thread);
        }
        executor.shutdown();
        executor.awaitTermination(60, TimeUnit.SECONDS);
        System.out.println(executor.getCompletedTaskCount()+" Completed Threads");
        System.out.println(executor.getActiveCount()+" Active Threads");
        System.out.println("Finished All Downloads!");
    }
}
